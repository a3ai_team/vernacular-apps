/* All services go here */
/* Kindly Keep code modular */
/* Use utlities whenever possible */

(function(){

    angular.module('quiz.services', ['quiz.utils'])

    .factory("eventsFactory", function($http, $q, UTILS, DOMAIN, apiUrls){

        var eventsFactory = {
            qaEvent: qaEvent,
        };

        function qaEvent(data){
            var url = DOMAIN.server + apiUrls.qaEvent;
            var defer = $q.defer();
            $http.post(url, data)
            .success(function(data, status, headers, config){
                defer.resolve([status, data]);
            })
            .error(function(data, status, headers, config){
                console.error(data);
                defer.reject([status, data]);
            });

            return defer.promise;
        }

        return eventsFactory;
    })

    .factory("cardService", function($http, $q, $localstorage, UTILS, DOMAIN, apiUrls){
        var cardService = {
            questionCards: [],
            getQuestionCards: getQuestionCards,
        };

        function getQuestionCards(){
            var url = DOMAIN.server + apiUrls.getQuestionCards;
            var defer = $q.defer();
            $http.get(url)
            .success(function(data, status, headers, config){
                defer.resolve([status, data]);
            })
            .error(function(data, status, headers, config){
                defer.reject([status, data]);
            });

            return defer.promise;
        }

        return cardService;        
    })

    .factory("authService", function($http, $q, $localstorage, UTILS, DOMAIN, apiUrls){
        var authService = {

            sessionKeys: ["key", "email", "name", "userID"],

            userSession: {
                name: null,
                email: null,
                key: null,
                userID: null,
            },
            auth: auth,
            setSession: setSession,
            checkSession: checkSession,
            setUserID: setUserID,
            getUserData: getUserData,
            getUserToken: getUserToken,
            logObject: logObject,
        };

        // auth api: assumes validated data
        function auth(obj){
            var defer = $q.defer();
            var data = {};

            var authRoute = (function(){
                if(obj.authFlag){
                    // register url
                    return DOMAIN.server + apiUrls.register;
                }
                else{
                    return DOMAIN.server + apiUrls.login;
                }
            })();

            // create auth payload
            if(obj.authFlag){
                // register
                data = {
                    "username": null,
                    "password1": obj.password,
                    "password2": obj.password,
                    "email": obj.email,
                };
            }
            else{
                // login payload
                data = {
                    "username": null,
                    "email": obj.email,
                    "password": obj.password,
                };
            }

            // set username
            data.username = (function(){
                var i = obj.email.indexOf("@");
                return obj.email.slice(0,i);
            })();

            console.log("auth payload", data);

            $http.post(authRoute, data)
            .success(function(data, status, headers, config){
                console.info("register/login success!", data, status);
                setSession(data);
                defer.resolve([data, status]);                
            })
            .error(function(data, status, headers, config){
                console.info("register/login fail!", data, status);
                defer.reject([data, status]);
            });

            return defer.promise;
        }


        /**** SESSION/LOCALSTORAGE RELATED FUNCTIONS ****/

        // util function used in setSession
        function setObject(key, object){
            if(authService.userSession.hasOwnProperty(key) && authService.userSession[key]){
                return authService.userSession[key];
            }
            else if(object.hasOwnProperty(key) && object[key]){
                return object[key];
            }
            else{
                return null;
            }
        }

        // sets user session after success callbacks of APIs
        // For any value to be set, an object with that property is passed as parameter 
        function setSession(object){
            
            // this object is passed to wrapper function that set localstorage
            var localstorageData = {};
            console.log("Setting session in authService ->" + JSON.stringify(object));
            
            // first check object fields and then set if property available
            // else set to value in authService.userSession 

            authService.sessionKeys.map(function(key){
                localstorageData[key] = setObject(key, object);
            });

            // set data in local storage 
            $localstorage.setObject("userData", {
                key: localstorageData.key,
                email: localstorageData.email,
                name: localstorageData.name,
                userID: localstorageData.userID,        
            });
            
        } // end of setSession

        // check if session info is available in localstorage
        // also set session in authService object
        function checkSession(){

            var defer = $q.defer();
            var userObject = $localstorage.getObject("userData");

            //console.log("checkSession: Checking localstorage for user data -> " + JSON.stringify(userObject));

            // if localstorage user_data available then set authService user_data object else resolve false promise
            // if data is unavailable it means localstorage has been deleted, app will have to contact server to fill session
            if (userObject.key){
                
                console.log("checkSession: Setting authService session object.")
                
                // check fields and then set authService session object

                if(userObject.hasOwnProperty("key")) authService.userSession.key = userObject.key;

                if(userObject.hasOwnProperty("email")) authService.userSession.email = userObject.email;

                if(userObject.hasOwnProperty("name")) authService.userSession.name = userObject.name;

                if(userObject.hasOwnProperty("userID")) authService.userSession.userID = userObject.userID;

                console.log("checkSession: authService.userSession->" + JSON.stringify(authService.userSession.user_data));
        
                defer.resolve(true);
            }
            else {
                defer.resolve(false);   
            }

            return defer.promise;

        } // end of checkSession

        function setUserID(userID){

            authService.userSession.userID = userID;

            var userObject = $localstorage.getObject("userData");

            if (!userObject.userID){
                console.info("Setting userID in authService");
                checkSession();
                authService.setSession({userID: userID});
            }

        }

        // returns user session data
        function getUserData(){

            var defer = $q.defer();

            var userObject = $localstorage.getObject("userData");

            console.info("userData", JSON.stringify(userObject));

            var data = {
                name: null,
                phone: null,
                email: null,
                address: null,
            };

            if(userObject.name){
                data = userObject;                
                defer.resolve(data);
            }
            else{
                defer.resolve(false);
            }

            return defer.promise;
        }

        // returns user token
        function getUserToken(){

            var defer = $q.defer();
            var userObject = $localstorage.getObject("userData");                        

            if(userObject.key){
                var data = userObject.key;                
                defer.resolve(data);
            }
            else{
                defer.resolve(false);
            }

            return defer.promise;
        }

        // returns stringified version of an obj
        // helps in log checking in terminal
        function logObject(obj){
            return JSON.stringify(obj);
        }

        return authService;

    })

;})()