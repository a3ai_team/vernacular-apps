
(function(){

	angular.module('quiz.directives', [])

	.directive('quizCard', function(){
	  	return {
	    	restrict: 'E',
	    	scope: {
	    		type: "=type",
	    		currentCard: "=currentCard",
	    		allCards: "=allCards",
	    	},
	    	templateUrl: function(e, attr){
	    		// fetch card type and select partiular card directive	    		
	    		var type = attr.type;
	    		var cardPath = "../templates/cards/";

	    		if(type==="quiz"){
	    			return cardPath + 'quizCard.html';	
	    		}
	    		
	    	},
	    	controller: function ($scope, eventsFactory){

	    		var cardsQ = $scope.allCards;
	    		
	    		$scope.answerChoice = null;
	    		$scope.answerFlag = false;
	    		$scope.answerSelectedFlag = null;
	    		$scope.selectedAnswer = null;
	    		$scope.doneFlag = false;

	    		$scope.answer = function(){
	    			fireQaEvent($scope.currentCard, $scope.answerChoice);
	    			checkAnswer($scope.answerChoice);	    			
	    		}

	    		function fireQaEvent(card, answer){
	    			console.log(card, answer);
	    			var data = {
	    				"question_guid": card.question.guid,
	    				"answer_guid": answer.guid,
	    			};
	    			eventsFactory.qaEvent(data)
	    			.then(function(resolve){
	    				console.log(resolve);
	    			}, function(reject){
	    				console.error(reject);
	    			});
	    		}

	    		// answer checker
	    		function checkAnswer(ans){
	    			if(ans){
	    				setAnswerSelected(true);
	    				$scope.selectedAnswer = ans;
	    				var correctAnswer = $scope.currentCard.question.correct_answer;
		    			if(ans.guid==correctAnswer.guid){
		    				setAnswerFlag(true);
		    			}	    			
		    			else{
		    				setAnswerFlag(false);
		    			}
	    			}
	    			else{
	    				setAnswerSelected(false);
	    			}	 		
	    		}

	    		function setAnswerFlag(flag){
	    			$scope.answerFlag = flag;
	    		}

	    		function setAnswerSelected(flag){
	    			$scope.answerSelectedFlag = flag;
	    		}

	    		$scope.nextQuestion = function(){	    			
	    			loadNextQuestion();
	            }  

	            function loadNextQuestion(){
	            	if (cardsQ.length>0 && cardsQ.length===1){
	            		console.info("questions over!");
	            		$scope.doneFlag = true;
	            	}
	            	else{
	            		reset();
	            		cardsQ = cardsQ.splice(1, cardsQ.length);
	            		$scope.currentCard = cardsQ[0];		            		
	            	}
	            	
	            }

	            function reset(){
	            	$scope.answerChoice = null;
		    		$scope.answerFlag = false;
		    		$scope.answerSelectedFlag = null;
		    		$scope.selectedAnswer = null;
	            }
	    	},	    	
	  	};

	})

	.directive('rightAnswer', function(){
	  	return {
	    	restrict: 'E',
	    	scope: {
	    		answer: "=answer", 
	    		selectedAnswer: "=selectedAnswer",   		
	    	},
	    	templateUrl: "../templates/cards/rightAnswer.html",
	    	controller: function ($scope){
	    		
	    	},	    	
	  	};

	})

	.directive('wrongAnswer', function(){
	  	return {
	    	restrict: 'E',
	    	scope: {
	    		answer: "=answer", 
	    		selectedAnswer: "=selectedAnswer",   		
	    	},
	    	templateUrl: "../templates/cards/wrongAnswer.html",	    	
	    	controller: function ($scope){
	    		
	    	},	    	
	  	};

	})


;})();
