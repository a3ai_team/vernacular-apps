(function(){

angular.module('quiz.utils', [])

.factory('$localstorage', ['$window', function($window) {
  return {
    set: function(key, value) {
      $window.localStorage[key] = value;
    },
    get: function(key, defaultValue) {
      return $window.localStorage[key] || defaultValue;
    },
    setObject: function(key, value) {
      $window.localStorage[key] = JSON.stringify(value);
    },
    getObject: function(key) {
      return JSON.parse($window.localStorage[key] || '{}');
    }
  }
}])

// utility functions
.factory("UTILS", function(){
  var o = {

    validateEmail : function(email){
      regex = /[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/igm;
      return regex.test(email);
    },

    validateText : function(text){
      if(text == undefined || text.length == 0 || text == " ")
        return false
      else return true;
    },

    validatePhone : function(phone){
      regex = /^\d{10}$/
      return regex.test(phone);
    },

    validatePassword : function(text){
      return (!(text===undefined || text === null || text.length<6));
    },

    validateNumber : function(number){
      number = parseInt(number, 10);
      if(number == undefined || number.length == 0 || number == " " || !(typeof number === 'number')){
        return false;
      }
      else return true;
    },

  };
  return o;
})

// filter to capfirst text
.filter('capfirst', function() {
    return function(input, scope) {
      if (input == undefined || input == null)
        return;
        return input.substring(0,1).toUpperCase()+input.substring(1);
      }
})

// log factory for debugging
.factory("LOG", function(DEBUG){
  var obj = {
    debug : DEBUG.Value,

    log : function(str){
      if (obj.debug){
        console.log(str);
      }
    }
  };
  return obj;
})

;

})();