/* All controllers go in this file */
/* Keep controllers as slim as possible */
/* Separate out as much as possible and include make services out of them. */ 
/* Naming convention for controllers: 'nameController'. A standard convention will make it easier to search for required controllers. */



(function(){

    angular.module('quiz.controllers', ["quiz.services"])

    .controller('AppCtrl', [
        "$scope", 
        "populateSession",
        "$state",
        function($scope, populateSession, $state) {
            //console.log("appCtrl controller loaded!", populateSession);
            if(!populateSession){
                $state.go("auth");
            }
        }
    ])

    .controller('homeController', [
        "$scope", 
        "cardService",
        "$compile",
        function($scope, cardService, $compile) {

            $scope.currentQuizCard = null;
            $scope.quizCards = [];
            $scope.loadingFlag = true;

            // init quiz cards
            (function init(){

                function quizCardsInit(quizCards){
                    if (quizCards && Array.isArray(quizCards) && quizCards.length>0){
                        $scope.quizCards = quizCards;
                        $scope.currentQuizCard = quizCards[0];    
                    }
                }

                cardService.getQuestionCards()
                .then(function(resolve){

                    $scope.loadingFlag = false;
                    quizCardsInit(resolve[1]);
                    // add quiz cards directive
                    angular.element(document.getElementById("cardsDiv"))
                    .append($compile("<quiz-card all-cards='quizCards' current-card='currentQuizCard' type='quiz'></quiz-card>")($scope));

                }, function(reject){
                    console.error(reject);
                });                

            })();
        }
    ])

    .controller('authController', [
        "$scope", 
        "authService",
        "UTILS",
        "$state",
        function($scope, authService, UTILS, $state){

            $scope.form = {
                email: null,
                password: null,
                authFlag: true,
                errors: [],
                errorFlag: false,
            };

            $scope.auth = function(){
                return validateForm(auth, {
                    email: $scope.form.email,
                    password: $scope.form.password,
                    authFlag: $scope.form.authFlag,
                });
            }

            function validateForm(authFunction, obj){

                clearErrors();
                $scope.form.errorFlag = (!UTILS.validateEmail(obj.email) || !UTILS.validatePassword(obj.password));                

                var errors = {};
                // check email
                if (!UTILS.validateEmail(obj.email)){
                    errors.email = ["Please enter a valid email!"];               
                }

                // check password
                if (!UTILS.validatePassword(obj.password)){
                    errors.password = ["Please enter at least 6 characters in your password!"];
                }                

                // set errors
                if($scope.form.errorFlag){
                    setErrors(errors);
                }

                return (!$scope.form.errorFlag && authFunction({
                    email: obj.email,
                    password: obj.password,
                    authFlag: obj.authFlag,
                }));
            }

            function auth(obj){

                // clear errors
                clearErrors();
                
                var data = {
                    email: obj.email,                    
                    authFlag: obj.authFlag,
                    password: obj.password,
                };

                authService.auth(data)
                .then(function(resolve){
                    console.log(resolve);
                    $state.go("app.home");
                }, function(reject){
                    console.error(reject);
                    // set errors
                    var errors = reject[0];
                    setErrors(errors);                  
                });
            }

            function clearErrors(){
                $scope.form.errorFlag = false;
                $scope.form.errors = [];
            }

            // sets errors in auth page
            // accepts an object with arrays for a particular type of error
            // {email: [e1, e2, ...], password: [e1, e2,...], non_field_errors: []e1, e2, ...}
            function setErrors(errors){

                console.log("errors", errors);
                $scope.form.errorFlag = true;

                // appends all strings of an array with <br> in between
                function fetchAllErrors(errors, key){
                    if(errors.hasOwnProperty(key)){
                        var e = errors[key].reduce(function(p, c, cI, array){
                            return p + "<br>" + c;
                        });
                        return e;
                    }
                    else{
                        return null;
                    }                    
                }

                // check for all error cases

                if (errors.hasOwnProperty("email")){
                    $scope.form.errors.push(fetchAllErrors(errors, "email"));
                }
                if (errors.hasOwnProperty("password")){
                    $scope.form.errors.push(fetchAllErrors(errors, "password"));
                }
                if (errors.hasOwnProperty("non_field_errors")){
                    $scope.form.errors.push(fetchAllErrors(errors, "non_field_errors"));
                }
            }

        }
    ])

    
;})();