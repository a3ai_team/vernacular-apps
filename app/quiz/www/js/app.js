// Ionic quiz App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'quiz' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'quiz.controllers' is found in controllers.js

(function(){

    angular.module('quiz', [
        'ionic', 
        'quiz.controllers',
        'quiz.services',
        'quiz.utils', 
        'quiz.directives',
        'ngCookies',
    ])

    .run(function($ionicPlatform) {
        $ionicPlatform.ready(function() {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);

            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }
        });

    })

    .config(function($interpolateProvider, $httpProvider){
        $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
         
    })

    // CSRF token setting
    .run(function($http, $cookies, authService){
        $http.defaults.headers.common['X-CSRFToken'] = $cookies['csrftoken'];
        authService.getUserToken()
        .then(function(key){            
            $http.defaults.headers.common['Authorization'] = "Token " + key;            
        });  
    })

    .config(function($stateProvider, $urlRouterProvider) {
        $stateProvider

        .state('app', {
            url: '/app',
            abstract: true,
            templateUrl: 'templates/menu.html',
            controller: 'AppCtrl',
            // don't load state untill user is populated
            resolve: {
                populateSession: function(authService){
                    return authService.checkSession();
                }
            },     

            onEnter: function($state, authService){
                authService.checkSession()
                .then(function(sessionFlag){
                    if(sessionFlag){
                        $state.go("app.home");
                    }
                    else{
                        $state.go("auth");
                    }
                });
            },

        })

        .state('auth', {
            url: "/auth",
            templateUrl: "templates/auth.html",
            controller: "authController",           
        })

        .state('app.home', {
            url: '/home',
            views: {
                'menuContent': {
                templateUrl: 'templates/home.html',
                controller: 'homeController'
                }
            },
        })

        ;
        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/app/home');

    })

    // Add development/testing/staging server domains
    .constant("DOMAIN", {

        // devlopment server domains
        server: "http://127.0.0.1:8000/",

        // production server domains
        server1: "",  

    })

    // All mobile api urls go here
    // DO NOT hard code these urls anywhere in the project
    .constant("apiUrls", {

        login: "rest-auth/login/",
        register: "rest-auth/registration/",  
        logout: "rest-auth/logout/",
        getQuestionCards: "api/cards/get_questions/",
        qaEvent: "api/quiz/qa_event/",
      
    })

    .constant("appConstants",{
      
    })

    ;

})()
