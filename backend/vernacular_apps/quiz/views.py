from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User

from rest_framework import generics
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework import status

from request_utils import check_token_and_user, parse_request, get_request_content

from quiz.models import AnswerChoice, Question, QuizEvent

import logging

logger = logging.getLogger(__name__)


@api_view(["POST"])
def test_api(request):	
	if not check_token_and_user(request):		
	 	return Response(status=status.HTTP_401_UNAUTHORIZED)
	
	return Response(status=status.HTTP_200_OK, data="Authenticated user: %s" %request.user)


@api_view(["POST"])
def qa_event(request):
	""" stores an question-answer event """
	
	if not check_token_and_user(request):
		return Response(status=status.HTTP_401_UNAUTHORIZED)

	data = parse_request(request)
	user = request.user
	question_guid = data.get("question_guid", None)
	answer_guid = data.get("answer_guid", None)
	question = None; answer = None;

	if question_guid:
		try:
			question = Question.objects.get(guid=question_guid)
		except Exception as e:
			logger.exception(e)
			question = None
	
	if answer_guid:
		try:
			answer = AnswerChoice.objects.get(guid=answer_guid)
		except Exception as e:
			logger.exception(e)
			answer = None	

	try:		

		quiz_event = QuizEvent.objects.create(
			user=user,
			question=question,
			answer_choice=answer,
			event_type="QA",
			)
	except Exception as e:
		logger.exception(e)
		return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR, data="Something went wrong: %s" %e)
	else:
		logger.debug("Quiz event type QA craeted: %s" %quiz_event)
		return Response(status=status.HTTP_200_OK, data="Quiz event saved: %s" %quiz_event.guid)
