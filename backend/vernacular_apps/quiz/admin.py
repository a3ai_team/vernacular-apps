from django.contrib import admin
from quiz.models import Category, Question, AnswerChoice, QuizEvent


class CategoryAdmin(admin.ModelAdmin):
	list_display = ("guid", "name", "description", "created_at")
	search_fields = ["guid", "name"]

admin.site.register(Category, CategoryAdmin)


class QuestionAdmin(admin.ModelAdmin):
	list_display = ("guid", "text", "category", "correct_answer", "created_at")
	search_fields = ["guid", "text"]

admin.site.register(Question, QuestionAdmin)	


class AnswerChoiceAdmin(admin.ModelAdmin):
	list_display = ("guid", "text", "question", "created_at")
	search_fields = ["guid", "text"]

admin.site.register(AnswerChoice, AnswerChoiceAdmin)

class QuizEventAdmin(admin.ModelAdmin):
	list_display = ("guid", "user", "question", "answer_choice", "created_at", "updated_at")
	search_fields = ["guid", "user"]

admin.site.register(QuizEvent, QuizEventAdmin)