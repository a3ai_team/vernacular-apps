from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User

import uuid

class Category(models.Model):
    """ categories for questions """

    guid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False, db_index=True)
    name = models.CharField(max_length=200, null=True, blank=False, unique= True)
    description = models.CharField(max_length=500, null=True,blank=True)
    
    created_at = models.DateTimeField(auto_now=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return str(self.guid)

    def __str__(self):
        return str(self.guid)


class Question(models.Model):
    """ question model """ 

    guid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False, db_index=True)
    text = models.CharField(max_length=500, null=True, blank=False)
    hint = models.CharField(max_length=200, null=True, blank=True)
    category = models.ForeignKey(Category, null=True, blank=True, related_name="category")
    correct_answer = models.OneToOneField('AnswerChoice', null=True, blank=True, related_name="correct_answer")
    
    created_at = models.DateTimeField(auto_now=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return str(self.guid)

    def __str__(self):
        return str(self.guid)


class AnswerChoice(models.Model):
    """ answer choices for a particular question. A question may may have any number 
        of choices out of which, 1 is correct """

    guid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False, db_index=True)
    text = models.CharField(max_length=200, null=True, blank=False)
    description = models.CharField(max_length=500, null=True, blank=True)
    question = models.ForeignKey('Question', null=True, blank=False, related_name="question")

    created_at = models.DateTimeField(auto_now=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return str(self.guid)

    def __str__(self):
        return str(self.guid)


class QuizEvent(models.Model):
    """ stores events of the quiz app such as answer selected for a question etc. """

    guid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False, db_index=True)
    question = models.ForeignKey(Question)
    answer_choice = models.ForeignKey(AnswerChoice)
    user = models.ForeignKey(User)
    event_type = models.CharField(max_length=100, null=True, blank=True)

    created_at = models.DateTimeField(auto_now=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return str(self.guid) + self.user.email

    def __str__(self):
        return str(self.guid) + self.user.email


