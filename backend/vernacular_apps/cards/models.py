from __future__ import unicode_literals
from django.db import models
from quiz.models import Question, AnswerChoice
from django.conf import settings
import uuid
import logging

logger = logging.getLogger(__name__)

class Image(models.Model):
    image = models.ImageField(upload_to="images/")
    title = models.CharField(max_length=100, null=True, blank=True)
    alt = models.CharField(max_length=100, null=True, blank=True)
    key = models.CharField(max_length=100, null=True, blank=False)

    created_at = models.DateTimeField(auto_now=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.key

    def process_image_json(self):
        return {
            "url": self.__get_absolute_url(),
            "title": self.title,
            "alt": self.alt,
            "key": self.key,
        }

    def __get_absolute_url(self):
        if not settings.DEBUG:
            return settings.MEDIA_URL + self.image.url
        else:
            return "http://127.0.0.1:8000" + self.image.url


class InformationCard(models.Model):
    """ cards that would engage user in a page """

    title1 = models.CharField(max_length=100, null=True, blank=True)
    title2 = models.CharField(max_length=100, null=True, blank=True)
    description = models.CharField(max_length=1000, null=True, blank=True)    
    images = models.ManyToManyField(Image, related_name="info_card_images", null=True, blank=True)

    color = models.CharField(max_length=100, null=True, blank=True)

    created_at = models.DateTimeField(auto_now=True)
    updated_at = models.DateTimeField(auto_now_add=True)


class QuestionCard(models.Model):
    """ cards that would contain questions with options """

    question = models.ForeignKey(Question, null=True, blank=False)
    is_active = models.BooleanField(default=True, null=False, blank=True)

    color = models.CharField(max_length=100, null=True, blank=True)
    images = models.ManyToManyField(Image, related_name="question_card_images", null=True, blank=True)
    
    created_at = models.DateTimeField(auto_now=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    def get_json(self):
        question = self.question
        payload = {
            "type": "quiz",
            "question": None,
            "images": None,
        }

        # set question attributes
        payload["question"] = self.__prepare_question_payload(question)
        payload["images"] = self.__prepare_images_payload()
        logger.debug("Question card payload: %s" %payload)
        return payload

    def __prepare_images_payload(self):
        payload = {}
        # check for linked images
        images = self.images.all()

        if (len(images)>0):
            for i in images:
                payload[i.key] = i.process_image_json()
            #payload = map(lambda i: i.process_image_json(), images)
            return payload
        else:
            return None

    def __prepare_question_payload(self, question):
        if question is None:
            return None
        payload = {
            "guid": question.guid,
            "text": question.text,
            "hint": question.hint,            
            "answer_choices": None,
            "correct_answer": None,            
        }
        payload["correct_answer"] = self.__prepare_correct_answer_payload(question.correct_answer)
        payload["answer_choices"] = self.__prepare_answer_choices_payload(question)        
        return payload

    def __prepare_correct_answer_payload(self, correct_answer):
        if correct_answer is None: 
            return None
        a = correct_answer
        payload = {
            "guid": a.guid,
            "text": a.text,
            "description": a.description,            
        }
        return payload

    def __prepare_answer_choices_payload(self, q):        
        if q is None:
            return None
        answer_choices = AnswerChoice.objects.filter(question=q)
        if len(answer_choices)>0:
            answer_choices_payload = map(self.__process_answer_choices, answer_choices)
            return answer_choices_payload

    def __process_answer_choices(self, a):
        if not a:
            return None
        return {
            "guid": a.guid,
            "text": a.text,
            "description": a.description,
        }


class AnswerCard(models.Model):

    correct_answer = models.ForeignKey(AnswerChoice, null=True, blank=False)
    is_active = models.BooleanField(default=True, null=False, blank=True)

    created_at = models.DateTimeField(auto_now=True)
    updated_at = models.DateTimeField(auto_now_add=True)


