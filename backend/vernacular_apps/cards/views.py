from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib import auth
from request_utils import check_token_and_user, parse_request, get_request_content

from rest_framework import generics
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework import status

from cards.models import QuestionCard, InformationCard, AnswerCard

import logging
import json

logger = logging.getLogger(__name__)



@api_view(["POST"])
def test_api(request):	
	if not check_token_and_user(request):		
	 	return Response(status=status.HTTP_401_UNAUTHORIZED)
	
	return Response(status=status.HTTP_200_OK, data="Authenticated user: %s" %request.user)


@api_view(["GET"])
def get_question_cards(request):
	if not check_token_and_user(request):		
	 	return Response(status=status.HTTP_401_UNAUTHORIZED)

	question_cards = QuestionCard.objects.all()
	question_cards_json = map(lambda q: q.get_json(), question_cards)
	return Response(status=status.HTTP_200_OK, data=question_cards_json)