from django.contrib import admin
from cards.models import QuestionCard, AnswerCard, Image, InformationCard


class QuestionCardAdmin(admin.ModelAdmin):
	list_display = ("question", "is_active", "created_at", "updated_at")
	search_fields = ["is_active"]

admin.site.register(QuestionCard, QuestionCardAdmin)


class AnswerCardAdmin(admin.ModelAdmin):
	list_display = ("correct_answer", "is_active", "created_at", "updated_at")	
	search_fields = ["is_active"]

admin.site.register(AnswerCard, AnswerCardAdmin)


class InformationCardAdmin(admin.ModelAdmin):
	list_display = ("title1", "title2", "description", "created_at", "updated_at")
	

admin.site.register(InformationCard, InformationCardAdmin)


class ImageAdmin(admin.ModelAdmin):
	list_display = ("title", "key", "title", "created_at", "updated_at")

admin.site.register(Image, ImageAdmin)