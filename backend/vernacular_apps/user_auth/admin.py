from django.contrib import admin
from user_auth.models import UserProfile


class UserProfileAdmin(admin.ModelAdmin):
	list_display = ("user", "created_at", "updated_at",)
	search_fields = ["user"]

admin.site.register(UserProfile, UserProfileAdmin)
