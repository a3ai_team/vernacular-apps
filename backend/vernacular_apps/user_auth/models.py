from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from rest_framework.authtoken.models import Token
import logging

logger = logging.getLogger(__name__)

class UserProfile(models.Model):
    """ extension of User """
    
    user = models.OneToOneField(User, db_index=True, related_name="user")
    # first_name = models.CharField(max_length=100, null=True, blank=True)
    # last_name = models.CharField(max_length=100, null=True, blank=True)
    
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return "%s" %(self.user)

    def __str__(self):
        return "%s" %(self.user)


def user_creation_post_save(sender, instance, created, **kwargs):
    """ creates user Token and user profile: called on user post save """    
    
    if created:        
        token = Token.objects.create(user=instance)
        user_profile = UserProfile.objects.create(user=instance)
        logger.info("Created token and user profile for user %s : %s, %s" %(instance, token, user_profile))

post_save.connect(user_creation_post_save, sender=User)